/*******
 * Read input from cin
 * Use cout << ... to output your result to STDOUT.
 * Use cerr << ... to output debugging information to STDERR
 * ***/
#include <iostream>
#include <limits>
#include <sstream>
#include "exercise.hpp"

using namespace std;
// Shortcuts
#define gl(s) getline(std::cin, line);
#define fin(i,n) for (int i = 0; i < n; i++)
#define fin2(i,a,b) for (int i = a; i < b; i++)

#define KO cout << "KO";
#define OK cout << "OK";

void split2s(const string& s, string& s1, string& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = s.substr(0, sindex);
   s2 = s.substr(sindex+1, s.size());
}

void split2i(const string& s, int& s1, int& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = stoi(s.substr(0, sindex));
   s2 = stoi(s.substr(sindex+1, s.size()));
}

ContestExerciseImpl::ContestExerciseImpl() : Exercise() {}

void ContestExerciseImpl::main() {
   string line;
   gl(line);
    int pilote = stoi(line);   
   gl(line);
   int N = stoi(line);
   int place = pilote;
   fin(i, N) {
       gl(line);
       string s1, s2;
       split2s(line, s1, s2, " ");
       if (stoi(s1) == pilote) {
           if (s2 == "D") {
               place ++;
           }
           else if (s2 == "I") {
               KO
               return;
           }
       }
   }
   
   cout << place;
	/* Vous pouvez aussi effectuer votre traitement une fois que vous avez lu toutes les données.*/
}