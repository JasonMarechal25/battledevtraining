/*
Objectif

L'animation d'une colonie de vacances suppose parfois de la préparation. Dans votre groupe, les enfants ayant entre 5 ans (inclus) et 9 ans (inclus) vont participer à l'activité toboggan. Vous connaissez l'age de tous les enfants et vous cherchez à déterminer combien d'enfants vont participer à l'activité.

Données

Entrée

Ligne 1 : un entier N compris entre 3 et 100 représentant le nombre d'enfants.
Ligne 2 : une suite de N entiers compris entre 1 et 20 séparés par des espaces représentant les âges des enfants.

Sortie

Un entier, indiquant le nombre d'enfants participant à l'activité toboggan.

Exemple

Pour l'entrée :
13
2 15 8 4 10 11 6 6 12 3 9 9 8

La sortie est :
6

Car il y a 6 enfants entre 5 et 9 ans (8, 6, 6, 9, 9, 8 dans la liste des entrées).
*/

/*******
 * Read input from cin
 * Use cout << ... to output your result to STDOUT.
 * Use cerr << ... to output debugging information to STDERR
 * ***/
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>
#include <algorithm>
#include "exercise.hpp"

using namespace std;
// Shortcuts
#define gl(s) getline(std::cin, line);
#define fin(i,n) for (int i = 0; i < n; i++)
#define fin2(i,a,b) for (int i = a; i < b; i++)

#define KO cout << "KO";
#define OK cout << "OK";

void split2s(const string& s, string& s1, string& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = s.substr(0, sindex);
   s2 = s.substr(sindex+1, s.size());
}

void split2i(const string& s, int& s1, int& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = stoi(s.substr(0, sindex));
   s2 = stoi(s.substr(sindex+1, s.size()));
}

void splitNs(vector<string>& vs, const string& input, const char del) {
    auto sindex = input.find(del);
    if (sindex == string::npos) {
        vs.push_back(input);
        return;
    }
        
    auto s1 = input.substr(0, sindex);
    vs.push_back(s1);
    auto ninput = input.substr(sindex+1, input.size());
    splitNs(vs, ninput, del);
}

void splitNi(vector<int>& vi, const string& input, const char del) {
    vector<string> vs;
    splitNs(vs, input, del);
    std::transform(vs.begin(), vs.end(), std::back_inserter(vi),
                   [](string s) -> int { return stoi(s); });
}

ContestExerciseImpl::ContestExerciseImpl() : Exercise() {}

void ContestExerciseImpl::main() {
   string line;
   gl(line);
    auto N = stoi(line);
    gl(line);
    vector<int> v;
    splitNi(v, line, ' ');
    int c = 0;
    int h = 0;
    cerr << v.size() << endl;
    fin2(i, 0, N) {
        cerr << i << ":" << v[i] << endl;
        if (v[i] >= 5 && v[i] <= 9) {
            ++c;
            cerr << c << endl;
        }
        cerr << "nop" << endl;
    }
    
    cerr << "plop" << endl;
    cout << c << endl;
	/* Vous pouvez aussi effectuer votre traitement une fois que vous avez lu toutes les données.*/
}
