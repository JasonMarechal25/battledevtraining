/*******
 * Read input from cin
 * Use cout << ... to output your result to STDOUT.
 * Use cerr << ... to output debugging information to STDERR
 * ***/
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>
#include <algorithm>
#include "exercise.hpp"

using namespace std;
// Shortcuts
#define gl(s) getline(std::cin, line);
#define fin(i,n) for (int i = 0; i < n; i++)
#define fin2(i,a,b) for (int i = a; i < b; i++)

#define KO cout << "KO";
#define OK cout << "OK";

void split2s(const string& s, string& s1, string& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = s.substr(0, sindex);
   s2 = s.substr(sindex+1, s.size());
}

void split2i(const string& s, int& s1, int& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = stoi(s.substr(0, sindex));
   s2 = stoi(s.substr(sindex+1, s.size()));
}

void splitNs(vector<string>& vs, const string& input, const char del) {
    auto sindex = input.find(del);
    if (sindex == string::npos) {
        vs.push_back(input);
        return;
    }
        
    auto s1 = input.substr(0, sindex);
    vs.push_back(s1);
    auto ninput = input.substr(sindex+1, input.size());
    splitNs(vs, ninput, del);
}

void splitNi(vector<int>& vi, const string& input, const char del) {
    vector<string> vs;
    splitNs(vs, input, del);
    std::transform(vs.begin(), vs.end(), std::back_inserter(vi),
                   [](string s) -> int { return stoi(s); });
}

ContestExerciseImpl::ContestExerciseImpl() : Exercise() {}

void ContestExerciseImpl::main() {
   string line;
   gl(line);

   OK
	/* Vous pouvez aussi effectuer votre traitement une fois que vous avez lu toutes les données.*/
}
