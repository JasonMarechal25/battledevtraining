/*Objectif

L'organisation d'un hackathon n'est pas une chose facile, quand on sait la logistique à mettre en œuvre pour que chacun ait une prise électrique, sans parler des bornes Wifi qui doivent tenir la charge ! C'est pourquoi vous vous attelez à la tâche avec une attention scrupuleuse au moindre détail. Tout naturellement, vous avez donc demandé à tous les participants de votre événement de renseigner leur heure d'arrivée et de départ à la milliseconde près. Ainsi, vous allez pouvoir déterminer à l'avance le pic de fréquentation et donc les moyens nécessaires. En effet, inutile de prévoir autant de prises électriques que d'inscrits, il suffit qu'il y en ait autant que le plus grand nombre de personnes simultanément présentes.

Données

Entrée

Ligne 1 : un entier N entre 2 et 100 000 inclus, indiquant le nombre de participants (et oui, votre hackathon est très populaire !)
Lignes 2 à N+1 : deux entiers compris entre 0 et 86 400 000 séparés par une espace, indiquant l'heure d'arrivée et l'heure de départ d'un même participant en nombre de millisecondes depuis le début de l'événement.

On vous garantit que parmi tous les horaires qu'on vous donne, il n'y en a jamais deux égaux. Ainsi il n'y aura jamais deux départs simultanés, deux arrivées simultanées, ou un départ et une arrivée simultanés.

Sortie

Un entier, indiquant le pic de fréquentation, c'est-à-dire le nombre maximum de personnes présentes au hackathon au même moment.

Exemple

Entrée

3
0 3600000
1800000 7200000
4800000 6000000

Sortie

2

Le premier participant arrive pile à l'heure et part après 3600000ms. Le deuxième arrive 1800000ms après le début — le premier est donc toujours là — puis s'en va 7200000ms après le début — le premier est alors déjà parti. Le troisième arrive après l'arrivée du deuxième et le départ du premier, et repart avant le deuxième.

Le pic de fréquentation est dans ce cas 2, il est atteint entre 1800000 ms et 3600000 ms (le premier et le second sont là), puis entre 4800000 ms et 6000000 ms (le second et le troisième sont là).*/
/*******
 * Read input from cin
 * Use cout << ... to output your result to STDOUT.
 * Use cerr << ... to output debugging information to STDERR
 * ***/
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>
#include <algorithm>
#include "exercise.hpp"

using namespace std;
// Shortcuts
#define gl(s) getline(std::cin, line);
#define fin(i,n) for (int i = 0; i < n; i++)
#define fin2(i,a,b) for (int i = a; i < b; i++)

#define KO cout << "KO";
#define OK cout << "OK";

void split2s(const string& s, string& s1, string& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = s.substr(0, sindex);
   s2 = s.substr(sindex+1, s.size());
}

void split2i(const string& s, int& s1, int& s2, const string& del) {
   auto sindex = s.find(del);
   s1 = stoi(s.substr(0, sindex));
   s2 = stoi(s.substr(sindex+1, s.size()));
}

void splitNs(vector<string>& vs, const string& input, const char del) {
    auto sindex = input.find(del);
    if (sindex == string::npos) {
        vs.push_back(input);
        return;
    }
        
    auto s1 = input.substr(0, sindex);
    vs.push_back(s1);
    auto ninput = input.substr(sindex+1, input.size());
    splitNs(vs, ninput, del);
}

void splitNi(vector<int>& vi, const string& input, const char del) {
    vector<string> vs;
    splitNs(vs, input, del);
    std::transform(vs.begin(), vs.end(), std::back_inserter(vi),
                   [](string s) -> int { return stoi(s); });
}

ContestExerciseImpl::ContestExerciseImpl() : Exercise() {}

void ContestExerciseImpl::main() {
   string line;
   gl(line);
   int N = stoi(line);
   int b = 0;
   vector<int> va, vd;
   fin(i, N) {
      gl(line);
      int A, D;
      split2i(line, A, D, " ");
      va.push_back(A);
      vd.push_back(D);
   }
   sort(va.begin(), va.end()); sort(vd.begin(), vd.end());
   
   int pick = 0;
   int max = 0;
   int j = 0;
   fin2(i, 0, vd.size()){
       int nextD = vd[i];
       for (j; j < va.size(); ++j) {
           if (va[j] < nextD) ++pick;
           else {
               break;
           }
       }
       if (pick > max) max = pick;
       pick--;
   }

cout << max;

	/* Vous pouvez aussi effectuer votre traitement une fois que vous avez lu toutes les données.*/
}
